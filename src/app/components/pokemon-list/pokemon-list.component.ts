import { Component, OnInit } from '@angular/core';
import { Pokemon } from '../../models/pokemon';
import { PokemonService } from '../../services/pokemon.service';

@Component({
  selector: 'cool-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.scss']
})
export class PokemonListComponent implements OnInit {

  public name = 'Pikachu'
  public pokemons: Pokemon[] = []

  public selectedPokemon = '';

  constructor(private pokemonService: PokemonService) { }

  ngOnInit(): void {
    this.pokemonService.getPokemons().subscribe((pokemons) => {
      this.pokemons = pokemons;
    });
  }

  onPokemonSelected(pokemon: Pokemon) {
    this.selectedPokemon = pokemon.name
  }

  onBtnClick() {
    this.pokemons[0].name = "Pickachu"
  }

  onClick() {
    console.log('click')
  }

}
