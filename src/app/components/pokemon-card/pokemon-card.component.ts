import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Pokemon } from '../../models/pokemon';

@Component({
  selector: 'cool-pokemon-card',
  templateUrl: './pokemon-card.component.html',
  styleUrls: ['./pokemon-card.component.scss']
})
export class PokemonCardComponent {

  @Input() public pokemon!: Pokemon;

  @Output() private pokemonSelected = new EventEmitter<Pokemon>();

  constructor() { }

  onClick() {
    this.pokemonSelected.emit(this.pokemon);
  }
}
